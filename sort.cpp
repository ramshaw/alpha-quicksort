#include <iostream>
using namespace std;

class Node {
    string w;
    public:
        int compare( Node );
        void set( string );
        void printWord() { cout << w << "\n"; }
        string getString() { return w; }
};

int Node::compare( Node s ) {
    return w.compare( s.getString() );
}

void Node::set( string s ) {
    w = s;
}

// Quicksort algorithm
int partition( Node a[], int lo, int hi );
void quicksort( Node a[], int lo, int hi );
int choosePivot( Node a[], int lo, int hi );
void swap( Node a[], int x, int y );

void quicksort( Node a[], int lo, int hi ) {
    if( lo < hi ) {
      int p = partition( a, lo, hi );
      quicksort( a, lo, p-1 );
      quicksort( a, p+1, hi );
    }
}

int partition( Node a[], int lo, int hi ) {
    int pivotIndex = choosePivot( a, lo, hi );
    Node pivot = a[pivotIndex];
    swap( a, pivotIndex, hi );
    int storeIndex = lo;
    int i;
    for( i = lo; i < hi; i++ ) {
        if( a[i].compare( pivot ) < 0 ) {
           swap( a, i, storeIndex );
           storeIndex++;
        }
    }
    swap( a, storeIndex, hi );
    return storeIndex;
}

void swap( Node a[], int x, int y ) {
    Node temp = a[x];
    a[x] = a[y];
    a[y] = temp;
}

// Just give hi as pivot, other pivots give worse times with alphabet.
int choosePivot( Node a[], int lo, int hi ) {
    return hi;
}

int main() {
    Node arr[240000] = {}; // approx length of unix words
    int i;
    string w;
    while( cin >> w ) {
        Node n;
        n.set(w);
        arr[i] = n;
        i++;
    }
    quicksort( arr, 0, i-1 );
    int n;
    for( n = 0; n < i; n++ ) {
        arr[n].printWord();
    }
    return 0;
}
